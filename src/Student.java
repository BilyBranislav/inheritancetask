import java.util.*;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 22.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Student extends Person {

    private String isicID;
    private LinkedHashMap<String, Double> marks;

    public Student(String name, String surname, String address, Calendar birthday, String isicID) {
        super(name, surname, address, birthday);
        this.isicID = isicID;
        marks = new LinkedHashMap<>();
    }

    public String getIsicID() {
        return isicID;
    }

    public void setIsicID(String isicID) {
        this.isicID = isicID;
    }

    public LinkedHashMap<String, Double> getMarks() {
        return marks;
    }

    public void setMarks(LinkedHashMap<String, Double> marks) {
        this.marks = marks;
    }

    /**
     * @param subject, which mark we want to find
     * @return found mark, if subject is not found return -1
     */
    public double getMarkBySubject(String subject) {
        double mark = -1;
        for (Map.Entry<String, Double> entry : marks.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            if (key.equals(subject)) {
                mark = value;
            }
        }
        return mark;
    }

    public Set<String> getSubjectsByMarkLowenThan(double mark) {
        Set<String> subjects = new LinkedHashSet<>();
        for (Map.Entry<String, Double> entry: marks.entrySet()) {
            if(entry.getValue() < mark) {
                subjects.add(entry.getKey());
            }
        }
        return subjects;
    }

    public Set<String> getSubjectsByMarkHigherThan(double mark) {
        Set<String> subjects = new LinkedHashSet<>();
        for (Map.Entry<String, Double> entry: marks.entrySet()) {
            if(entry.getValue() > mark) {
                subjects.add(entry.getKey());
            }
        }
        return subjects;

    }

    public void addSubject(String subject, double mark) {
        marks.put(subject, mark);
    }

    @Override
    public void showInfo() {
        super.showInfo();
        showStudentSpecificInfo();
    }

    public double getGPA() {
        double GPA = 0;
        for (Map.Entry<String, Double> entry : marks.entrySet()) {
            GPA += entry.getValue();
        }
        return (double) Math.round(GPA / marks.size() * 100) / 100;
    }

    public void showStudentSpecificInfo() {
        String info = "Occupation: Student" + "\n" + "IsicID: " + getIsicID() +
                      "\n" + "Marks: " + marks.toString() + "\n";
        System.out.println(info);
    }


}
