import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 22.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class ListOfPersons {

    private Set<Person> persons;

    ListOfPersons() {
        persons = new LinkedHashSet<>();
    }

    public boolean addPerson(Person person) {
        try {
            persons.add(person);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean removePerson(Person personToRemove) {
        return persons.removeIf(person -> person.equals(personToRemove));
    }

    public boolean removePersonByID(String id) {
        for (Person person :
                persons) {
            if(isStudent(person)) {
                if(((Student) person).getIsicID().equals(id)) {
                    persons.remove(person);
                    return true;
                }
            } else {
                if(((Teacher) person).getTeacherID().equals(id)) {
                    persons.remove(person);
                    return true;
                }
            }
        }
        return false;
    }

    public void showAllInfo() {
        for (Person person :
                persons) {
            person.showInfo();
        }
    }

    public void showStudents() {
        for (Person person :
                persons) {
            if (person instanceof Student) {
                person.showInfo();
            }
        }
    }

    public void showTeachers() {
        for (Person person :
                persons) {
            if (person instanceof Teacher) {
                person.showInfo();
            }
        }
    }

    public Set<Student> getStudents() {
        Set<Student> students = new LinkedHashSet<>();
        for (Person person :
             persons) {
            if(isStudent(person)) {
                students.add((Student) person);
            }
        }
        return students;
    }

    public Set<Teacher> getTeachers() {
        Set<Teacher> teachers = new LinkedHashSet<>();
        for (Person person :
                persons) {
            if(isTeacher(person)) {
                teachers.add((Teacher)person);
            }
        }
        return teachers;
    }

    public boolean isStudent(Person person) {
        return person instanceof Student;
    }

    public boolean isTeacher(Person person) {
        return person instanceof Teacher;
    }

    public Set<Student> getStudentsByGPALessThan(double GPA) {
        Set<Student> students = new LinkedHashSet<>();
        for (Student student:
             getStudents()) {
            if(student.getGPA() < GPA) {
                students.add(student);
            }
        }
        return students;
    }
}
