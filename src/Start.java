import java.util.*;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 22.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Start {

    public static void main(String[] args) {

        Calendar birthday = Calendar.getInstance();
        birthday.set(2000, Calendar.MARCH, 6);
        Student brano = new Student("Brano", "Bily", "Kupelna 1", birthday, "S421000104038P");
        brano.addSubject("Math", 2.1);
        brano.addSubject("Physics", 2.6);

        birthday = Calendar.getInstance();
        birthday.set(2001, Calendar.DECEMBER, 21);
        Student dominik = new Student("Dominik", "Borbuliak", "Centrum Niekde", birthday, "#SDASDSADASD@SDADASDASD");
        dominik.addSubject("Math", 1.2);
        dominik.addSubject("Physics", 4);

        birthday = Calendar.getInstance();
        birthday.set(1992, Calendar.APRIL, 5);

        ClassRoom maths = new ClassRoom();
        maths.addStudent(brano);

        Teacher roland = new Teacher("Tibor", "Roland", "Fajezna 1",
                birthday, "F3400001040321D", new HashSet<>(Arrays.asList("Maths", "Physics")), maths);

        birthday = Calendar.getInstance();
        birthday.set(1996, Calendar.APRIL, 30);

        Teacher duri = new Teacher("Duri", "Nezname", "Sidlisko III",
                birthday, "F3400001040321D", new HashSet<>(Collections.singletonList("Programming")), maths);

        ListOfPersons listOfPersons = new ListOfPersons();
        listOfPersons.addPerson(roland);
        listOfPersons.addPerson(dominik);
        listOfPersons.addPerson(brano);
        listOfPersons.addPerson(duri);
        listOfPersons.showAllInfo();
//        listOfPersons.showAllInfo();

//        listOfPersons.showStudents();
//        listOfPersons.showTeachers();
//        Set<Student> students = listOfPersons.getStudentsByGPALessThan(5);
////        for (Student student :
////                students) {
////            student.showInfo();
////        }
    }
}
