import java.util.Calendar;
import java.util.Set;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 22.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Teacher extends Person {

    private String teacherID;
    private Set<String> courses;
    private ClassRoom assignedClass;

    public Teacher(String name, String surname, String address, Calendar birthday, String teacherID, Set<String> courses, ClassRoom assignedClass) {
        super(name, surname, address, birthday);
        this.teacherID = teacherID;
        this.courses = courses;
        this.assignedClass = assignedClass;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public Set<String> getCourses() {
        return courses;
    }

    public void setCourses(Set<String> courses) {
        this.courses = courses;
    }

    public ClassRoom getAssignedClass() {
        return assignedClass;
    }

    public void Classroom (ClassRoom assignedClass) {
        this.assignedClass = assignedClass;
    }

    @Override
    public void showInfo() {
        super.showInfo();
        System.out.println("Occupation: Teacher");
        showCourses();
    }

    public void showCourses() {
        StringBuilder sb = new StringBuilder();
        sb.append("Teacher of: ");
        for (String course :
                courses) {
            sb.append(course).append(" ");
        }
        System.out.println(sb.toString() + "\n");
    }
}
