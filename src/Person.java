import java.util.Calendar;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 22.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class Person {
    private String name;
    private String surname;
    private String address;
    private Calendar birthday;

    public Person(String name, String surname, String address, Calendar birthday) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public void showInfo() {
        Calendar calendar = getBirthday();
        String sb = ("Name: " + getName()) + "\n" + "Surname: " + getSurname() +
                "\n" + "Address: " + getAddress() +
                "\n" + "Birthday: " + calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(sb);
    }
}
