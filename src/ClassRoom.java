import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 22.11.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */
public class ClassRoom {
    private Set<Student> students;

    ClassRoom() {
        students = new LinkedHashSet<>();
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Student student : students) {
            result.append(student.getSurname()).append("\n");
        }
        return result.toString();
    }
}
